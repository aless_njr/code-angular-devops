# CodeAngularDevops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

# Pasos previos
Tener instalado un Editor de texto (En nuestro caso se uso Visual code).
Tener o crear cuenta de Gitlab (puede ser cuenta gratuita).
Crear un proyecto vacio.

# Detalle de los pasos a seguir

## 0 Crear proyecto
Esto se realizo desde Visual Code
ng new code-angular-devops

## 1 Abrir terminal en la ruta del proyecto ejecutar:
git remote add origin https://gitlab.com/aless_njr/code-angular-devops.git

## 2 Abrir terminal en la ruta del proyecto ejecutar:
git push -u origin master

## 3 configuracion de las pruebas se debe colocar en el archivo karma
browsers: ['Chrome'],
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},

## 4 Configuracion en packege.json el cual sirve para correr pruebas
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",

## 5 comando para correr las pruebas de integración continua en el servidor
npm run test:ci

## 6 Crear archivo .gitlab-ci.yml para definir script de pasos a seguir para la integracion continua
copiar el archi .gitlab-ci.yml.

## 7 se agregara en una rama diferente a la master esto
image: node:10.16.0

stages:
  - install
  - test
  - build
  - deploy

install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/


tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'

## 8 para la rama master se agrega adicional a lo anterior esto es para que hace el build y deploy
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master

pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/codelab-angular-devops/* ./public/
    - cp public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master

## 9 Debe colocar esto en el archivo package.json antes de hacer commit.
"build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/code-angular-devops/",